package com.muhardin.endy.training.automatedtest.aplikasibukutamu.dao;

import com.muhardin.endy.training.automatedtest.aplikasibukutamu.entity.Tamu;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.List;
import javax.sql.DataSource;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=SpringBootTest.WebEnvironment.RANDOM_PORT)
@Sql(scripts = {
    "/sql/reset-data-tamu.sql", "/sql/sample-tamu.sql"
})
@IfProfileValue(name = "spring.profiles.active", value = "default")
public class TamuDaoTests {
    
    @Autowired private DataSource dataSource;
    @Autowired private TamuDao tamuDao;
    
    @Test
    public void testCariTamuBerdasarkanNama(){
        String nama = "User 001";
        
        List<Tamu> hasilQuery = tamuDao.findTamuByNamaOrderByNama(nama);
        
        /*
        for(Tamu t : hasilQuery){
            System.out.println("Nama : "+t.getNama());
            System.out.println("Email : "+t.getEmail());
            System.out.println("No HP : "+t.getNoHp());
        }
        */
        
        Assert.assertTrue("Data hasil query harusnya cuma ada satu record", hasilQuery.size() == 1);
        Tamu t1 = hasilQuery.get(0);
        
        Assert.assertEquals(nama, t1.getNama());
        Assert.assertEquals("user001@gmail.com", t1.getEmail());
        Assert.assertEquals("081234567890", t1.getNoHp());
    }
    
    @Test
    public void cariDataTamu(){
        Page<Tamu> dataTamu = tamuDao.findAll(PageRequest.of(0, 10));
        Assert.assertNotNull(dataTamu);
        Assert.assertEquals(1, dataTamu.getTotalElements());
    }
    
    @Test
    public void simpanDataBaruSukses() throws Exception {
        Tamu t = new Tamu();
        t.setNama("Tamu 001");
        t.setEmail("t001@coba.com");
        t.setNoHp("0811111111");
        t.setAlamat("Jakarta Timur");
        
        tamuDao.save(t);
        
        String sql = "select * from tamu where email = 't001@coba.com'";
        
        Connection koneksiDb = dataSource.getConnection();
        ResultSet hasilQuery = koneksiDb.createStatement().executeQuery(sql);
        
        Boolean adaRecord = hasilQuery.next();
        Assert.assertTrue(adaRecord);
        
        Assert.assertEquals("Tamu 001", hasilQuery.getString("nama"));
        Assert.assertEquals("t001@coba.com", hasilQuery.getString("email"));
        Assert.assertEquals("0811111111", hasilQuery.getString("no_hp"));
        Assert.assertEquals("Jakarta Timur", hasilQuery.getString("alamat"));
        
        hasilQuery.close();
        koneksiDb.close();
    }
}
