package com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest;

import com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest.pageobject.FormInputTamu;
import java.util.List;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.web.client.RestTemplate;

@RunWith(JUnitParamsRunner.class)
@IfProfileValue(name = "spring.profiles.active", value = "default")
public class InputTamuValidasiTests {

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();


    private static final String XPATH_PESAN_ERROR_NAMA = "/html/body/form/div[1]/div";
    private static final String XPATH_PESAN_ERROR_EMAIL = "/html/body/form/div[2]/div";
    private static final String XPATH_PESAN_ERROR_HP = "/html/body/form/div[3]/div";
    
    private static WebDriver webdriver;
    
    @BeforeClass
    public static void sebelumSemuaTest(){
        System.out.println("Method sebelum semua test");
        
        FirefoxOptions opsi = new FirefoxOptions();
        //opsi.setHeadless(true);
        webdriver = new FirefoxDriver(opsi);
    }
    
    @AfterClass
    public static void sesudahSemuaTest(){
        System.out.println("Method setelah semua test");
        
        webdriver.quit();
    }

    @Before
    public void resetDataTamu() {
        new RestTemplate().delete("http://localhost:8080/tamu/reset");
    }
    
    @Test
    public void testInputTamuPageObject(){
        webdriver.get("http://localhost:8080/tamu/list");
        String judulHalaman = webdriver.getTitle();
        Assert.assertEquals("Data Tamu", judulHalaman);
        
        WebElement linkTambahData = webdriver.findElement(By.linkText("Tambah Data Baru"));
        Assert.assertNotNull(linkTambahData);
        linkTambahData.click();
        
        new WebDriverWait(webdriver,5).until((wd) -> {
            return wd.getTitle().equals("Edit Tamu");
        });
        
        // inisialisasi page object
        FormInputTamu form = new FormInputTamu(webdriver);
        
        String nama = "ab";
        String email = "aa";
        
        form.isiNama(nama);
        form.isiEmail(email);
        TestHelper.ambilScreenshot(webdriver);
        
        form.simpan();
        
        // tunggu halamannya loading selama 5 detik, setelah itu cek apakah datanya ada
        new WebDriverWait(webdriver,5).until(ExpectedConditions.titleIs("Edit Tamu"));
        
        TestHelper.ambilScreenshot(webdriver);
        WebElement pesanErrorNama = webdriver.findElement(By.xpath(XPATH_PESAN_ERROR_NAMA));
        Assert.assertNotNull(pesanErrorNama);
        Assert.assertTrue(pesanErrorNama.getText().contains("size must be between 3 and 255"));
    }
    
    @Test
    @FileParameters("src/test/resources/csv/test-input-tamu.csv")
    public void testInputTamuParameterized(String nama, String email, String hp, String alamat, 
            Boolean sukses, String idElemenPesanError, String textPesanError){
        webdriver.get("http://localhost:8080/tamu/form");
        
        new WebDriverWait(webdriver,5).until(ExpectedConditions.titleIs("Edit Tamu"));
        
        // inisialisasi page object
        FormInputTamu form = new FormInputTamu(webdriver);
        
        form.isiNama(nama);
        form.isiEmail(email);
        form.isiHp(hp);
        form.isiAlamat(alamat);
        TestHelper.ambilScreenshot(webdriver);
        
        form.simpan();
        
        TestHelper.ambilScreenshot(webdriver);
        
        if(sukses) {
            new WebDriverWait(webdriver,5).until(ExpectedConditions.titleIs("Data Tamu"));
            
            List<WebElement> hasil = webdriver.findElements(By.xpath("/html/body/table/tbody/tr/td[1]"));
            Assert.assertNotNull(hasil);
            Assert.assertFalse(hasil.isEmpty());
            
            Boolean dataMasuk = false;
            for(WebElement dataTamu : hasil){
                if(dataTamu.getText().equals(nama));
                dataMasuk = true;
                break;
            }
            
            Assert.assertTrue(dataMasuk);
        } else {
            WebElement pesanError = webdriver.findElement(By.id(idElemenPesanError));
            Assert.assertNotNull(pesanError);
            Assert.assertTrue(pesanError.getText().contains(textPesanError));
        }
       
    }
}
