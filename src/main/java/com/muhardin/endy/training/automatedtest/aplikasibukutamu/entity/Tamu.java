package com.muhardin.endy.training.automatedtest.aplikasibukutamu.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import lombok.Data;

@Entity @Data
public class Tamu {
    
    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    @NotEmpty @Size(min = 3, max = 255)
    private String nama;
    
    @NotEmpty @Email
    private String email;
    
    @NotEmpty @Size(min = 5)
    private String noHp;
    private String alamat;
}
