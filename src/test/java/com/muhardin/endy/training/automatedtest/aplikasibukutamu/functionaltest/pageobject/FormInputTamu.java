package com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest.pageobject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class FormInputTamu {
    
    @FindBy(name = "nama")
    private WebElement inputNama;
    
    @FindBy(name = "email")
    private WebElement inputEmail;
    
    @FindBy(name = "noHp")
    private WebElement inputHp;
    
    @FindBy(name = "alamat")
    private WebElement inputAlamat;
    
    @FindBy(name = "simpan")
    private WebElement tombolSimpan;
    
    public FormInputTamu(WebDriver webDriver){
        PageFactory.initElements(webDriver, this);
    }
    
    public void isiNama(String nama){
        inputNama.sendKeys(nama);
    }
    
    public void isiEmail(String nama){
        inputEmail.sendKeys(nama);
    }
    
    public void isiHp(String nama){
        inputHp.sendKeys(nama);
    }
    
    public void isiAlamat(String nama){
        inputAlamat.sendKeys(nama);
    }
    
    public void simpan(){
        tombolSimpan.click();
    }
}
