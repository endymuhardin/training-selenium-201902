# Training Selenium 201902 #

[![Konsep Automated Test](docs/img/konsep-automated-test.jpg)](docs/img/konsep-automated-test.jpg)

## Cara menjalankan Remote Test Selenium ##

* Download Selenium Server [di websitenya](https://www.seleniumhq.org/download/)

* Salah satu komputer berfungsi sebagai Hub, jalankan dengan perintah :

    ```
    java -jar selenium-server-standalone-*.jar -role hub
    ```

    Outputnya seperti ini

    ```
    15:16:42.325 INFO [GridLauncherV3.parse] - Selenium server version: 3.141.59, revision: e82be7d358
    15:16:42.518 INFO [GridLauncherV3.lambda$buildLaunchers$5] - Launching Selenium Grid hub on port 4444
    2019-06-20 15:16:43.221:INFO::main: Logging initialized @1544ms to org.seleniumhq.jetty9.util.log.StdErrLog
    15:16:43.619 INFO [Hub.start] - Selenium Grid hub is up and running
    15:16:43.622 INFO [Hub.start] - Nodes should register to http://10.10.81.87:4444/grid/register/
    15:16:43.622 INFO [Hub.start] - Clients should connect to http://10.10.81.87:4444/wd/hub
    ```

* Komputer lain menjalankan Selenium Server sebagai Node dengan perintah :

    ```
    java -jar selenium-server-standalone-*.jar -role node -hub http://10.10.81.87:4444/grid/register/
    ```

* Hasilnya bisa dilihat di Selenium Server Console seperti ini

    [![Selenium Server Console](docs/img/selenium-server-console.png)](docs/img/selenium-server-console.png)

## Referensi ##

* [Tutorial Selenium Headless](http://www.automationtestinghub.com/selenium-headless-chrome-firefox/)
* [Laravel Dusk : Menggunakan Selenium untuk Functional Test di aplikasi Laravel](https://laravel.com/docs/5.8/dusk)
* [Tutorial Laravel Dusk](https://buddy.works/guides/how-run-laravel-dusk-selenium-tests)
* [Menjalankan Laravel Dusk di Gitlab CI](https://gist.github.com/cerw/d46c33297f1fc0f4f778b4e9d2ec4329)
* [Docker Image berisi Laravel Dusk dan MySQL](https://hub.docker.com/r/chilio/laravel-dusk-ci/)
