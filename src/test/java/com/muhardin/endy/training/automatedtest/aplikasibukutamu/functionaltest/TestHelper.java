package com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.springframework.util.FileCopyUtils;

public class TestHelper {
    public static void ambilScreenshot(WebDriver webdriver) {
        try {
            // screenshot dulu, untuk melihat isian yang dilakukan script
            File screenshot = ((TakesScreenshot) webdriver).getScreenshotAs(OutputType.FILE);
            Path screenshotFolder = Paths.get("target","screenshots");
            screenshotFolder.toFile().mkdirs();
            String namaFileScreenshot = screenshotFolder.toFile().getAbsolutePath() + File.separator + UUID.randomUUID().toString()+".png";
            System.out.println("Menulis screenshot ke file : "+namaFileScreenshot);
            FileCopyUtils.copy(screenshot, new File(namaFileScreenshot));
        } catch (Exception err) {
            err.printStackTrace();
        }
    }
}
