package com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest;

import com.github.javafaker.Faker;
import com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest.pageobject.FormInputTamu;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.client.RestTemplate;

import java.util.Locale;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@IfProfileValue(name = "spring.profiles.active", value = "default")
public class InputTamuTests {
    
    private static WebDriver webdriver;
    private Faker faker = new Faker(new Locale("id", "id"));
    
    @LocalServerPort
    private Integer portAplikasi;
    
    @BeforeClass
    public static void sebelumSemuaTest(){
        System.out.println("Method sebelum semua test");
        
        FirefoxOptions opsi = new FirefoxOptions();
        opsi.setHeadless(true);
        webdriver = new FirefoxDriver(opsi);
    }
    
    @AfterClass
    public static void sesudahSemuaTest(){
        System.out.println("Method setelah semua test");
        
        webdriver.quit();
    }
    
    @Before
    public void sebelumTiapTest(){
        System.out.println("Method sebelum tiap test");
        new RestTemplate().delete("http://localhost:"+portAplikasi+"/tamu/reset");
    }
    
    @After
    public void setelahTiapTest(){
        System.out.println("Method setelah tiap test");
    }
    
    //@Test
    public void testInputTamuNormal() {
        System.out.println("Test input tamu normal");
        
        webdriver.get("http://localhost:"+ portAplikasi +"/tamu/list");
        String judulHalaman = webdriver.getTitle();
        Assert.assertEquals("Data Tamu", judulHalaman);
        
        WebElement linkTambahData = webdriver.findElement(By.linkText("Tambah Data Baru"));
        Assert.assertNotNull(linkTambahData);
        
        linkTambahData.click();
        
        judulHalaman = webdriver.getTitle();
        Assert.assertEquals("Edit Tamu", judulHalaman);
        
        String namaDepan = faker.name().firstName();
        String namaBelakang = faker.name().lastName();
        
        // isi nama
        WebElement inputNama = webdriver.findElement(By.name("nama"));
        Assert.assertNotNull(inputNama);
        inputNama.sendKeys(namaDepan + " "+namaBelakang);
        
        // isi email
        WebElement inputEmail = webdriver.findElement(By.name("email"));
        Assert.assertNotNull(inputEmail);
        inputEmail.sendKeys(faker.internet().emailAddress((namaDepan+"."+namaBelakang).toLowerCase()));
        
        // isi hp
        WebElement inputHp = webdriver.findElement(By.name("noHp"));
        Assert.assertNotNull(inputHp);
        inputHp.sendKeys(faker.phoneNumber().cellPhone());
        
        // isi hp
        WebElement inputAlamat = webdriver.findElement(By.name("alamat"));
        Assert.assertNotNull(inputAlamat);
        inputAlamat.sendKeys(faker.address().fullAddress());
        
        TestHelper.ambilScreenshot(webdriver);
        
        // klik tombol simpan
        WebElement tombolSimpan = webdriver.findElement(By.name("simpan"));
        tombolSimpan.click();
        
        // tunggu halamannya loading
        WebDriverWait wait = new WebDriverWait(webdriver,30);
        wait.until((t) -> {
            return t.findElements(By.xpath("/html/body/table/tbody/tr/td[1]"))
                    .stream()
                    .anyMatch((e) -> (e.getText().equals(namaDepan + " "+namaBelakang)));
        });
        
    }

    
    
    @Test
    public void testInputTamuPageObject(){
        webdriver.get("http://localhost:"+ portAplikasi +"/tamu/list");
        String judulHalaman = webdriver.getTitle();
        Assert.assertEquals("Data Tamu", judulHalaman);
        
        WebElement linkTambahData = webdriver.findElement(By.linkText("Tambah Data Baru"));
        Assert.assertNotNull(linkTambahData);
        linkTambahData.click();
        
        new WebDriverWait(webdriver,5).until((wd) -> {
            return wd.getTitle().equals("Edit Tamu");
        });
        
        // inisialisasi page object
        FormInputTamu form = new FormInputTamu(webdriver);
        
        String namaDepan = faker.name().firstName();
        String namaBelakang = faker.name().lastName();
        String nama = namaDepan + " " + namaBelakang;
        String email = faker.internet().emailAddress((namaDepan + "." + namaBelakang).toLowerCase());
        
        form.isiNama(nama);
        form.isiEmail(email);
        form.isiHp(faker.phoneNumber().cellPhone());
        form.isiAlamat(faker.address().fullAddress());
        TestHelper.ambilScreenshot(webdriver);
        
        form.simpan();
        
        // tunggu halamannya loading selama 5 detik, setelah itu cek apakah datanya ada
        new WebDriverWait(webdriver,5).until((t) -> {
            return t.findElements(By.xpath("/html/body/table/tbody/tr/td[1]"))
                    .stream()
                    .anyMatch((e) -> (e.getText().equals(nama)));
        });
    }
    
    @Test
    public void testInputTamuNamaInvalid(){
        System.out.println("Test input tamu dengan nama invalid");
    }
}
