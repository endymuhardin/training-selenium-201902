package com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest;

import com.muhardin.endy.training.automatedtest.aplikasibukutamu.functionaltest.pageobject.FormInputTamu;
import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.test.context.junit4.rules.SpringClassRule;
import org.springframework.test.context.junit4.rules.SpringMethodRule;
import org.springframework.web.client.RestTemplate;

import java.net.URL;
import java.util.List;
import org.openqa.selenium.chrome.ChromeOptions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.env.Profiles;

@RunWith(JUnitParamsRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
@IfProfileValue(name = "spring.profiles.active", values = {"localgrid", "gitlabci"})
public class SeleniumGridRemoteTests {

    @ClassRule
    public static final SpringClassRule SPRING_CLASS_RULE = new SpringClassRule();

    @Rule
    public final SpringMethodRule springMethodRule = new SpringMethodRule();

    @Value("${hostname.selenium}")
    private String hostnameSelenium;
    @Value("${hostname.aplikasi}")
    private String hostnameAplikasi;

    @LocalServerPort
    private Integer portAplikasi;
    
    @Autowired private Environment env;

    private static WebDriver webdriver;
    
    @Before
    public void sebelumSemuaTest() throws Exception {

        if(webdriver == null) {
            ChromeOptions opsi = new ChromeOptions();
            //opsi.setCapability(CapabilityType.PLATFORM_NAME, Platform.WINDOWS);
            opsi.setHeadless(true);
            //InternetExplorerOptions opsi = new InternetExplorerOptions();

            String urlSeleniumHub = "http://"+hostnameSelenium+":4444/wd/hub";
            webdriver = new RemoteWebDriver(new URL(urlSeleniumHub), opsi);
        }
    }
    
    @AfterClass
    public static void sesudahSemuaTest(){
        if(webdriver != null) {
            webdriver.quit();
        }
    }

    @Before
    public void resetDataTamu() {
        if(env.acceptsProfiles(Profiles.of("gitlabci"))){
            portAplikasi = 80;
        }
        
        new RestTemplate()
                .delete("http://"+hostnameAplikasi+":"+portAplikasi+"/tamu/reset");
    }

    @Test
    @FileParameters("src/test/resources/csv/test-input-tamu.csv")
    public void testInputTamuParameterized(String nama, String email, String hp, String alamat, 
            Boolean sukses, String idElemenPesanError, String textPesanError){
        
        String urlAplikasi = "http://"
                +hostnameAplikasi+":"
                +portAplikasi
                +"/tamu/form";
        System.out.println("URL Aplikasi : "+urlAplikasi);
        webdriver.get(urlAplikasi);
        
        new WebDriverWait(webdriver,5).until(ExpectedConditions.titleIs("Edit Tamu"));
        
        // inisialisasi page object
        FormInputTamu form = new FormInputTamu(webdriver);
        
        form.isiNama(nama);
        form.isiEmail(email);
        form.isiHp(hp);
        form.isiAlamat(alamat);
        TestHelper.ambilScreenshot(webdriver);
        
        form.simpan();
        
        TestHelper.ambilScreenshot(webdriver);
        
        if(sukses) {
            new WebDriverWait(webdriver,5).until(ExpectedConditions.titleIs("Data Tamu"));
            
            List<WebElement> hasil = webdriver.findElements(By.xpath("/html/body/table/tbody/tr/td[1]"));
            Assert.assertNotNull(hasil);
            Assert.assertFalse(hasil.isEmpty());
            
            Boolean dataMasuk = false;
            for(WebElement dataTamu : hasil){
                if(dataTamu.getText().equals(nama));
                dataMasuk = true;
                break;
            }
            
            Assert.assertTrue(dataMasuk);
        } else {
            WebElement pesanError = webdriver.findElement(By.id(idElemenPesanError));
            Assert.assertNotNull(pesanError);
            Assert.assertTrue(pesanError.getText().contains(textPesanError));
        }
       
    }
}
